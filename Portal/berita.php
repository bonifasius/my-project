<?php 
	require_once("koneksi.php");
?>

<h2>Table Berita</h2>
<table border="1" cellpadding="5">

	<tr>
		<td>id</td>
		<td>kategori</td>
		<td>judul</td>
		<td>Isi</td>
		<td>Tanggal</td>
		<td>Aksi</td>
	</tr>

	<?php 
		$query = mysqli_query($con, "SELECT * FROM berita b inner join kategori k on b.id_kategori=k.id_kategori");
		while($record = mysqli_fetch_array($query)){
		
	?>

	<tr>
		<td><?php echo $record["id_berita"];?></td>
		<td><?php echo $record["nama_kategori"];?></td>
		<td><?php echo $record["judul_berita"];?></td>
		<td><?php echo $record["isi_berita"];?></td>
		<td><?php echo $record["tanggal_berita"];?></td>
		<td>
			<a href="#">Update</a>
			<a href="#" onclick="return confirm('Mau hapus?')">Delete</a>
		</td>
	</tr>

	<?php 
		}
	?>
	<tr>
		<td colspan="5" align="center"><a href="tambahberita.php">Tambah</a></td>
	</tr>
</table>