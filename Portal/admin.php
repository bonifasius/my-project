<?php 
	require_once("koneksi.php");
?>

<h2>Table Admin</h2>
<table border="1" cellpadding="10">

	<tr>
		<td>id</td>
		<td>username</td>
		<td>password</td>
		<td>Aksi</td>
	</tr>

	<?php 
		$query = mysqli_query($con, "SELECT * FROM admin");
		while($record = mysqli_fetch_array($query)){
		
	?>

	<tr>
		<td><?php echo $record["id_admin"];?></td>
		<td><?php echo $record["username"];?></td>
		<td><?php echo $record["password"];?></td>
		<td>
			<a href="updateadmin.php?id_admin=<?php echo $record['id_admin']?>">Update</a>
			<a href="deleteadmin.php?id_admin=<?php echo $record['id_admin']?>" class="btn btn-xs btn-danger" onClick="return confirm('Apakah anda yakin akan menghapus data ini?')">Delete</a> 
		</td>
	</tr>

	<?php 
		}
	?>
	<tr>
		<td colspan="4" align="center"><a href="tambahadmin.php">Tambah Admin</a></td>
	</tr>

</table>